using System;
using System.Reflection;
using Xunit;
using GenericsApp;

namespace TestFor5
{
    public class UnitTest
    {
        class IntegerWrapper : ICastable<int>
        {
            public int Value { get; set; }

            public int Cast()
            {
                return Value;
            }
        }

        [Fact]
        public void AddableArrayTest()
        {
            var doubleArray = new AddableArray<double>();
            Assert.True(doubleArray is IAddable<double[]>);
            Assert.Empty((double[])doubleArray.GetType().GetField("array", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(doubleArray));
            var intArray = new AddableArray<int>(new int[] { 1, 2, 3 });
            Assert.Equal(3, ((int[])intArray.GetType().GetField("array", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(intArray)).Length);
            var ints = new int[] { 4, 5, 6 };
            var newInts = intArray.AddTo(ints);
            Assert.Equal(4, newInts[0]);
            Assert.Equal(5, newInts[1]);
            Assert.Equal(6, newInts[2]);
            Assert.Equal(1, newInts[3]);
            Assert.Equal(2, newInts[4]);
            Assert.Equal(3, newInts[5]);
        }
    }
}
