using System;
using Xunit;
using GenericsApp;
using System.Reflection;
using System.Collections.Generic;

namespace TestFor7
{
    public class UnitTest
    {
        class AddableInteger : IAddable<int>
        {
            public int Value { get; set; }

            public AddableInteger(int value)
            {
                Value = value;
            }

            public int AddTo(int value)
            {
                return Value + value;
            }
        }

        [Fact]
        public void PackTest()
        {
            var pack = new Pack<int>();
            Assert.True(pack is IEnumerable<IAddable<int>>);
            Assert.Equal(0, pack.Sum());
            pack.Add(new AddableInteger(2));
            pack.Add(new AddableInteger(102));
            Assert.Equal(104, pack.Sum());
            pack.RemoveLast();
            Assert.Equal(2, pack.Sum());
            pack.RemoveLast();
            Assert.Throws<Exception>((Action)pack.RemoveLast);
            try
            {
                pack.RemoveLast();
            }
            catch (Exception e)
            {
                Assert.True(e.Message.Length > 0);
            }
            pack.Add(new AddableInteger(0));
            pack.Add(new AddableInteger(1));
            pack.Add(new AddableInteger(2));
            pack.Add(new AddableInteger(3));
            pack.Add(new AddableInteger(4));
            pack.RemoveLast();
            int index = 0;
            pack.Process(x => Assert.Equal(index++, x.AddTo(0)));
            pack.Add(new AddableInteger(4));
            index = 0;
            pack.Process(x => Assert.Equal(index++, x.AddTo(0)));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(2)]
        [InlineData(7)]
        public void UtilityTest(int value)
        {
            var arr = Utility.GetAddableArray<double>(value);
            var generated = (double[])arr.GetType().GetField("array", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(arr);
            Assert.Equal(value, generated.Length);
            for (int i = 0; i < value; ++i)
                Assert.Equal(0, generated[i]);

            arr = Utility.GetAddableArray(value, i => i + 0.1);
            generated = (double[])arr.GetType().GetField("array", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(arr);
            Assert.Equal(value, generated.Length);
            for (int i = 0; i < value; ++i)
                Assert.Equal(i + 0.1, generated[i]);

            var objArr = Utility.GetAddableArray<object>(value);
            var objGenerated = (object[])objArr.GetType().GetField("array", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(objArr);
            Assert.Equal(value, objGenerated.Length);
            for (int i = 0; i < value; ++i)
                Assert.Null(objGenerated[i]);
        }
    }
}
