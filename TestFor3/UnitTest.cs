using Xunit;
using GenericsApp;

namespace TestFor3
{
    public class UnitTest
    {
        class IntegerWrapper : ICastable<int>
        {
            public int Value { get; set; }

            public int Cast()
            {
                return Value;
            }
        }

        [Fact]
        public void CastableTest()
        {
            var castMethods = typeof(ICastable<int>).GetMethods();
            Assert.Single(castMethods);
            Assert.Equal("Int32 Cast()", castMethods[0].ToString());
        }

        [Fact]
        public void AddableTest()
        {
            var castMethods = typeof(IAddable<int>).GetMethods();
            Assert.Single(castMethods);
            Assert.Equal("Int32 AddTo(Int32)", castMethods[0].ToString());
        }

        [Theory]
        [InlineData(-100)]
        [InlineData(42)]
        public void BasicNumberTest(int value)
        {
            Number<IntegerWrapper> number = new Number<IntegerWrapper>(new IntegerWrapper());
            number.Value.Value = value;
            Assert.Equal(value, number.Value.Value);
            Assert.Equal(value, number.Cast());
            Assert.Equal(value + 200, number.AddTo(200));
            Assert.True(number is ICastable<int>);
            Assert.True(number is IAddable<int>);
        }
    }
}
